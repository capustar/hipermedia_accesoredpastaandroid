package com.rosadiez.pastalist;

import android.graphics.Bitmap;

import java.lang.ref.SoftReference;

public class Pasta
{
    private String mUrl;
    private String mName;
    SoftReference<Bitmap> mImagen;

    Pasta( String _url, String _name ) {
        mUrl = _url;
        mName = _name;
        mImagen = null;
    }

    public String getUrl() { return mUrl; }
    public void setUrl( String _url ) { mUrl = _url; }

    public String getName() { return mName; }
    public void setName( String _name ) { mName = _name; }

    public SoftReference<Bitmap> getImagen() { return mImagen; }
    public void setImagen(SoftReference<Bitmap> _bitmap ) { mImagen = _bitmap; }
}
