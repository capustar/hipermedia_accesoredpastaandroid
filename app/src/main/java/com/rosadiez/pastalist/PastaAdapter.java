package com.rosadiez.pastalist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PastaAdapter extends BaseAdapter implements AbsListView.OnScrollListener
{
    private List<Pasta> mList;
    private Context mContext;
    /* TODO: mapa de descargas activas */
    private Map<Pasta, CargarImagenTask> mImagenesCargando;
    private boolean mBusy;

    public PastaAdapter(Context context, List<Pasta> objects)
    {
        mContext = context;
        mList = objects;
        /* TODO: inicializar mapa de descargas activas */
        mImagenesCargando = new HashMap<Pasta, CargarImagenTask>();
        mBusy = false;
    }

    @Override
    public int getCount()
    {
        return mList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.list_item, null);
        }

        TextView tvTexto = (TextView) convertView.findViewById(R.id.textView);
        ImageView ivIcono = (ImageView) convertView.findViewById(R.id.imageView);

        Pasta pasta = mList.get(position);
        tvTexto.setText(pasta.getName());

        /* TODO: Completar la descarga lazy de imágenes */
        if(pasta.getImagen() != null)
        {
            ivIcono.setImageBitmap(pasta.getImagen().get());
        }
        else
        {
            // Ponemos esta imagen de momento
            ivIcono.setImageResource(R.drawable.ic_launcher_background);

            // Si la imagen no está descargando...
            if(mImagenesCargando.get(pasta) == null && !mBusy)
            {
                CargarImagenTask task = new CargarImagenTask();
                mImagenesCargando.put(pasta, task);
                task.execute(pasta, ivIcono);
            }
        }

        return convertView;
    }

    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount)
    {
    }

    public void onScrollStateChanged(AbsListView view, int scrollState)
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            // Do something for lollipop and above versions
        } else{
            // do something for phones running an SDK before lollipop
        }
        switch(scrollState)
        {
            case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                mBusy = false;
                notifyDataSetChanged();
                break;
            case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                mBusy = true;
                break;
            case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                mBusy = true;
                break;
        }
    }

    class CargarImagenTask extends AsyncTask<Object, Integer, Bitmap>
    {
        Pasta pasta;
        ImageView view;

        @Override
        protected Bitmap doInBackground(Object... params)
        {
            HttpURLConnection http = null;
            Bitmap bitmap = null;

            this.pasta = (Pasta)params[0];
            this.view = (ImageView)params[1];

            try
            {
                URL url = new URL( this.pasta.getUrl() );
                http = (HttpURLConnection)url.openConnection();

                if( http.getResponseCode() == HttpURLConnection.HTTP_OK )
                    bitmap = BitmapFactory.decodeStream(http.getInputStream());
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if( http != null )
                    http.disconnect();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result)
        {
            if(result!=null)
            {
                this.pasta.setImagen(new SoftReference<Bitmap>(result));
                this.view.setImageBitmap(result);
                Log.d("DESCARGA", "Se ha descargado una imagen");
            }
        }
    }
}
