package com.rosadiez.pastalist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetalleActivity extends AppCompatActivity
{
    private void i_rellenarDatos()
    {
        String nombre;
        TextView text_view_nombre;
        Pasta pasta;
        String url_imagen;
        ImageView image_view_imagen;

        nombre = getIntent().getStringExtra(getString(R.string.EXTRA_NOMBRE));
        text_view_nombre = findViewById(R.id.ad_textViewNombre);
        text_view_nombre.setText(nombre);

        url_imagen = getIntent().getStringExtra(getString(R.string.EXTRA_URL));
        pasta = new Pasta(url_imagen, nombre);

        image_view_imagen = findViewById(R.id.ad_imageViewImagen);
        new PastaLoader().execute(pasta, image_view_imagen);
        image_view_imagen.setImageBitmap(pasta.getImagen().get());
    }

// -----------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        i_rellenarDatos();
    }
}
