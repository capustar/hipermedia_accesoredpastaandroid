package com.rosadiez.pastalist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity
{
    ListView m_list_view;

// -----------------------------------------------------------------------------

    private void i_setItemClickListener()
    {
        m_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Intent intent;
                Pasta pasta;

                intent = new Intent(getApplicationContext(), DetalleActivity.class);

                pasta = (Pasta) m_list_view.getAdapter().getItem(i);
                intent.putExtra(getString(R.string.EXTRA_NOMBRE), pasta.getName());
                intent.putExtra(getString(R.string.EXTRA_URL), pasta.getUrl());
                startActivity(intent);
            }
        });
    }

// -----------------------------------------------------------------------------

    private void i_configurarLista()
    {
        List<Pasta> pastas;
        PastaAdapter adaptador_pasta;

        pastas = PastaList.getPasta();
        adaptador_pasta = new PastaAdapter(this, pastas);

        m_list_view.setAdapter(adaptador_pasta);
    }

// -----------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_list_view = findViewById(R.id.lista);
        i_configurarLista();
        i_setItemClickListener();
    }
}
